module PagesHelper

  def title(page_title)
    content_for :title, page_title.to_s
  end

  def description(page_description)
    content_for :description, page_description.to_s
  end

  def index_follow(page_index, page_follow)
    page_index == true ? page_index ="INDEX" : page_index = "NOINDEX"
    page_follow == true ? page_follow ="FOLLOW" : page_follow = "NOFOLLOW"
    content_for :index_follow, (page_index.to_s + ', ' + page_follow.to_s)
  end

  def follow(page_follow)
    content_for :follow, page_follow.to_s
  end

end
