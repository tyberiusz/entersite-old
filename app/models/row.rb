class Row < ApplicationRecord
  belongs_to :page
  has_many :columns, dependent: :destroy

  include RailsSortable::Model
  set_sortable :order 
end
