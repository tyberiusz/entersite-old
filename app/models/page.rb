class Page < ApplicationRecord

	has_many :rows, dependent: :destroy
	validates :slug, presence: true

	accepts_nested_attributes_for :rows
	
end
