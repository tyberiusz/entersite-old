class Column < ApplicationRecord
  belongs_to :row

  include RailsSortable::Model
  set_sortable :order 
end
