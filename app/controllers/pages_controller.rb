class PagesController < ApplicationController

	def index
    @pages = Page.all
  end

  def new
    @page = Page.new
  end

  def show
    @page = Page.includes(rows: [:columns]).find_by(slug: params[:slug])
    @rows = @page.rows.order(:order).all
  end

  def create
    @page = Page.new(page_params)
    
    if @page.save
      redirect_to edit_page_path(@page)
    else
      render 'new'
    end
  end

  def edit
    @page = Page.find(params[:id])
  end


  def update
    @page = Page.find(params[:id])
 
    if @page.update(page_params)
      redirect_to @page
    else
      render 'edit'
    end
  end

  def destroy
    @page = Page.find(params[:id])
    @page.destroy
   
    redirect_to :action => 'index'
  end

  private
  def page_params
    params.require(:page).permit(:title, :description, :slug, :follow, :index)
  end


end
