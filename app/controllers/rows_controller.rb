class RowsController < ApplicationController
  before_action :set_row, only: [:edit, :update, :show, :destroy]
  
	def new
		@row = Row.new
	end

  def create
    @page = Page.find(params[:id])
    rows_count = @page.rows.count + 1
    @row = Row.new(order: rows_count, html_class: 'white',  page_id: @page.id )
    @row.save!
    
    respond_to do |format|
    format.html { redirect_to edit_page_path(@row.page_id) }
    format.js
    end
  end

  def show ; end
  def edit ; end

  def update
  @row.update(row_params)

  respond_to do |format|
    format.html { redirect_to edit_page_path(@row.page_id) }
    format.js
    end
  end

  def destroy
  	@row.destroy

    respond_to do |format|
      format.html { redirect_to edit_page_path(@row.page_id) }
      format.js
	  end
  end

  private

  def set_row
    @row = Row.find(params[:id])
  end

  def row_params
    params.require(:rows).permit( :order, :html_class, :page_id)
  end


end
