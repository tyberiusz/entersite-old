class ColumnsController < ApplicationController



	def new
		
		@row = Row.find(params[:id])
		@column = Column.new
	end

  def create
    #@page = Page.find_by(params[:page_id])
	
	@row = Row.find(params[:id])
	@column = @row.columns.create(column_params)
	#@column.create("row_id" => "c_type_#{@row.id}")
	#@column = Column.new(column_params)
	#@row.columns.create(column_params)

	#@column = Column.new( row_id: @row.id )
	if @column.save!
		respond_to do |f|
	    f.html { redirect_to edit_page_path(@column.row.page_id) }
	    f.js
		end
	end

  end
 
  
  def show
  	@row = Row.find(params[:id])
  end

  def destroy
  	
  	@column = Column.find(params[:id])
  	@column.destroy

  	    respond_to do |f|
    f.html { redirect_to edit_page_path(@column.row.page_id) }
    f.js
	end

  end

  private
    def column_params
      params.require(:columns).permit(:order,									
      								:text_color,
									:background_color,
									:text_align,
									:vertical_align,
									:size_mobile,
									:tablet_size,
									:desktop_size,
									:smarttv_size,
									:hide_mobile,
									:hide_tablet,
									:hide_desktop,
									:row_id
									)
    end
end
