# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180303212014) do

  create_table "columns", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.bigint "row_id"
    t.integer "order"
    t.string "text_color"
    t.string "background_color"
    t.string "text_align"
    t.string "vertical_align"
    t.integer "size_mobile"
    t.integer "tablet_size"
    t.integer "desktop_size"
    t.integer "smarttv_size"
    t.boolean "hide_mobile"
    t.boolean "hide_tablet"
    t.boolean "hide_desktop"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["row_id"], name: "index_columns_on_row_id"
  end

  create_table "pages", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "title"
    t.text "description"
    t.string "slug"
    t.string "h1"
    t.boolean "index"
    t.boolean "follow"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "rows", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "html_class"
    t.integer "order"
    t.bigint "page_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["page_id"], name: "index_rows_on_page_id"
  end

  add_foreign_key "columns", "rows"
  add_foreign_key "rows", "pages"
end
