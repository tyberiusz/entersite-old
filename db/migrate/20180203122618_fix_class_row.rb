class FixClassRow < ActiveRecord::Migration[5.1]
  def change
  	rename_column :rows, :class, :html_class
  end
end
