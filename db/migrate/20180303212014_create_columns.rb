class CreateColumns < ActiveRecord::Migration[5.1]
  def change
    create_table :columns do |t|
      t.references :row, foreign_key: true
      t.integer :order
      t.string :text_color
      t.string :background_color
      t.string :text_align
      t.string :vertical_align
      t.integer :size_mobile
      t.integer :tablet_size
      t.integer :desktop_size
      t.integer :smarttv_size
      t.boolean :hide_mobile
      t.boolean :hide_tablet
      t.boolean :hide_desktop

      t.timestamps
    end
  end
end
