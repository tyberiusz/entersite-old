class CreatePages < ActiveRecord::Migration[5.1]
  def change
    create_table :pages do |t|
      t.string :title
      t.text :decription
      t.string :slug
      t.string :h1
      t.boolean :index
      t.boolean :follow

      t.timestamps
    end
  end
end
