Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  
get '/admin/pages/new', to: 'pages#new', as: :new
post 'admin/pages/:id', to: 'pages#create'
get '/admin/pages/:id/edit', to: 'pages#edit', as: :page
patch '/admin/pages/:id', to: 'pages#update'

post "/admin/pages/:id/rows/new" , to: "rows#create", as: :create_row
get "/admin/rows/:id/edit" , to: "rows#edit", as: :edit_row
post "/admin/rows/:id/edit" , to: "rows#update", as: :update_row
delete "/admin/rows/:id" , to: "rows#destroy", as: :delete_row


get "/admin/columns/:id/edit" , to: "columns#edit", as: :edit_column
delete "/admin/columns/:id" , to: "columns#destroy", as: :delete_column

get "/admin/rows/:id/columns/new" , to: "columns#new", as: :new_column
post "/admin/rows/:id/columns/new" , to: "columns#create", as: :create_column

delete "/admin/pages/:id" , to: "pages#destroy", as: :delete_page
#get "/admin/pages/:id/rows/:row_id/columns/new", to: "columns#new", as: :new_column
#post "/admin/pages/:id/rows/:row_id/columns/new", to: "columns#create", as: :create_column

#delete "/admin/columns/:id" , to: "column#destroy", as: :delete_column

scope '/admin' do
	resources :pages, except: [:show , :update] do
		resources :rows, except: [:create, :destroy] do
			resources :columns, except: [:create]
		end
		
	end
end
  
  root 'pages#index'

  get '/:slug', to: 'pages#show', as: :show

end




